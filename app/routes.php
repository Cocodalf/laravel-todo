<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/********** HOME *************/ 
Route::get('/', array('before' => 'auth', function()
{
	return View::make('site.index');
}));

/********** TEST *************/ 
Route::get('test', function()
{
	return 'juste un test';
});

/********** TEST AVEC VUE *************/ 
Route::get('test-view', function()
{
	return View::make('tests.view-01');
});

/********** 404 *************/ 
App::missing(function($exception){
    return Response::make('Page Introuvable', 404);
});

/********** VUES REGISTER *************/ 
Route::get('register', function(){
	return View::make('site.register');

});

Route::post('register', function(){
	DB::table('users')->insert(	
			array('username'=>Input::get('username'),'password' => Hash::make(Input::get('password')))
		);
	return Redirect::to('login');
});

/********** VUES LOGGIN *************/ 
Route::get('login', function(){
	return View::make('site.login');

});

Route::post('login', function(){
	$username = Input::get('username');
	$password = Input::get('password');

	if(Auth::attempt(array('username'=> $username, 'password'=> $password)) ){
		echo 'You are connected !'.Auth::user()->username;
		return Redirect::to('/');
	}
	else{
		echo "Connection fail !";
	}
});
/********** VUES LOGGOUT *************/ 

Route::get('logout', function(){
	Auth::logout();
	return Redirect::to('login');
});

/********** Todolist *************/ 

Route::get('todo', function(){
	$todos = Todo::all();
	return View::make('site.todo')->with('todos', $todos);
});

Route::post('todo', function(){
	$content=Input::get('content');
	$todo=new Todo;
	$todo->content=$content;
	$todo->save();
	return Redirect::to('/todo');
});

Route::delete('todo/{id}', function($id){
	$todo = Todo::find($id);
	$todo->delete();
	return Redirect::to('/todo');
});

Route::put('todo/{id}', function($id){
	$content = Input::get('newContent');
	$todo = Todo::find($id);
	$todo->content= $content;
	$todo->save();
	return Redirect::to('/todo');
});
