@extends('site.layout')
@section('page-title')
	TodoList
@stop

@section('content')
	<div class="panel-heading">
		<h1>Todolist Gang !!</h1>
	</div>
	<div class="panel-body">
		<h2>Bonjour {{ Auth::User()->username}} </h2>
		<nav class="nav navbar-default">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li> <a href="{{URL::to('/todo')}}">Todolist</a></li>
					<li> <a href="{{URL::to('/logout')}}">Logout</a></li>
				</ul>
			</div>
			
		</nav>
	</div>
		
@stop