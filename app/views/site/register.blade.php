<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<link rel="stylesheet" href="{{ asset(Config::get('template.AdminLTEPublicURL')) }}/bootstrap/css/bootstrap.min.css">
	<style type="text/css">
		body{
			text-align: center;
		}
		input{
			margin: 15pt;
		}
	</style>
</head>
<body>
	<div class="panel-heading">
		<h1>Register !!</h1>

	</div>
	<div class="panel-body">

		<form action="" method="POST">
			<input type="text" name="username"><br>
			<input type="text" name="password"><br>
			<button class="btn btn-success" method="submit"> Register</button>
	
		</form>
		<div>
			<a href="{{ URL::to('/login') }}"> Login</a>
		</div>
	</div>
		

</div>
<script src="{{ asset(Config::get('template.AdminLTEPublicURL')) }}/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

