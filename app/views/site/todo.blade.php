@extends('site.layout')
@section('page-title')
	TodoList
@stop

@section('content')
<nav class="nav navbar-default">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li> <a href="{{URL::to('/todo')}}">Todolist</a></li>
					<li> <a href="{{URL::to('/logout')}}">Logout</a></li>
				</ul>
			</div>
</nav>
	<form action="" method="POST">
		<input type="text" name="content">
		<button class="btn btn-success">Ajouter</button>
	</form>
	<div class="panel panel-default">

		<table class="table table-stripped">
			<thead>
				<th>Todo</th>
				<th>Supression</th>
				<th>Modification</th>
			</thead>
			<tbody>
						
				@foreach($todos as $todo)
					<tr>
						<td>
							<div>{{ $todo->content }}</div>
						</td>
						<td>
							<form action="{{URL::to('/todo',[$todo->id]) }}" method="POST">
								<input type="hidden" name="_method" value="DELETE">
								<button class="btn btn-danger">Supprimer</button>
							</form>
						</td>
						<td>
							<form action="{{URL::to('/todo',[$todo->id]) }}" method="POST">
								<input type="hidden" name="_method" value="PUT">
								<button class="btn btn-warning">Modifier</button>
								<input type="text" name="newContent">
							</form>
						</td>
					</tr>
				@endforeach

			</tbody>
		</table>
	</div>

@stop